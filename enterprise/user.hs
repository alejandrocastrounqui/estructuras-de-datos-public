import Enterprise
import Set

-- Propósito: Denota los tripulantes de la nave
tripulantes :: Nave -> Set Tripulante
tripulantes a = emptyS

-- Propósito: Elimina al tripulante de la nave.
-- Pista: Considere reconstruir la nave sin ese tripulante.
-- Opcional (Bonus): 
bajaDeTripulante :: Tripulante -> Nave -> Nave
bajaDeTripulante a b = b
