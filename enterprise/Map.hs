module Map(
  Map,
  emptyM,
  assocM,
  lookupM,
  deleteM,
  domM
) where

data Map k v = UnMap [(k, v)]
instance (Show k, Show v) => Show (Map k v) where
  show = showMap

intercalate :: (Show k, Show v) => String -> [(k,v)] -> String
intercalate _ [] = ""
intercalate _ [(k,v)] = show k ++ " -> " ++ show v
intercalate separator ((k,v):xs) = 
 (show k ++ " -> " ++ show v) ++ separator ++ (intercalate separator xs)


showMap :: (Show k, Show v) => Map k v -> String
showMap (UnMap xs) =  "{\n  " ++ (intercalate "\n  " xs) ++ "\n}"

-- Crea un map vacío.
emptyM :: Map k v
emptyM = UnMap []

assocM :: Ord k => k -> v -> Map k v -> Map k v
assocM k v (UnMap kv) = UnMap (insertL k v kv)

insertL :: Ord k => k -> v -> [(k,v)] -> [(k,v)]
insertL k v [] = [(k,v)]
insertL k v (kv:kvs) = 
  if fst kv == k
    then
      (k,v):kvs
    else
      kv:( insertL k v kvs)


lookupM :: Ord k => Map k v -> k -> Maybe v
lookupM (UnMap kv) p = lookupL kv p

lookupL [] p = Nothing
lookupL (kv:kvs) p = 
  if fst kv == p
    then
      Just $ snd kv
    else
      lookupL kvs p

deleteM :: Ord k => k ->Map k v -> Map k v
deleteM k (UnMap kv) = UnMap (deleteL k kv)

deleteL k [] = []
deleteL k (kv:kvs) = 
  if fst kv == k
    then
      kvs
    else
      kv:( deleteL k kvs)

domM :: Map k v -> [k]
domM (UnMap kv) = domL kv

domL [] = []
domL (kv:kvs) = 
    (fst kv):( domL kvs)