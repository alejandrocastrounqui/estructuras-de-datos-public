module Set(
  Set,
  emptyS,
  addS,
  belongs,
  sizeS,
  removeS,
  unionS,
  intersectionS,
  setToList,
  printSet
) where

-- el entero reprenta el tamanio de la lista
-- la lista no puede tener repetidos
data Set a = UnSet [a] Int


-- Crea un conjunto vacío.
emptyS :: Set a
emptyS = UnSet [] 0

-- O(n)
-- Dados un elemento y un conjunto, agrega el 
-- elemento al conjunto.
addS :: Eq a => a -> Set a -> Set a
addS x (UnSet xs size) = 
  if elem x xs
    then UnSet xs size
	else UnSet (x:xs) (size + 1)

-- O(n)
-- Dados un elemento y un conjunto indica si el 
-- elemento pertenece al conjunto.
belongs :: Eq a => a -> Set a -> Bool
belongs x (UnSet xs size) = elem x xs 

-- O(1)
-- Devuelve la cantidad de elementos distintos de 
-- un conjunto.
sizeS :: Eq a => Set a -> Int
sizeS (UnSet xs size) = size

-- O(n+m) = O(2n) = O(n)
-- Borra un elemento del conjunto.
removeS :: Eq a => a -> Set a -> Set a
removeS x (UnSet xs size) = 
  if elem x xs
    then UnSet (removeL x xs) (size-1)
	else (UnSet xs size)

-- Borra un elemento del conjunto.
removeS' :: Eq a => a -> Set a -> Set a
removeS' x (UnSet xs size) = 
    armarSet (removeL' x xs)

armarSet :: Eq a => [a] -> Set a
armarSet xs = UnSet xs (length xs)

-- precondicion: el elemento no esta repetido
removeL' :: Eq a => a -> [a] -> [a]
removeL' x [] = []
removeL' x (y:ys) = 
  if x == y
    then ys
	else y: (removeL' x ys)

-- precondicion: el elemento no esta repetido
-- precondicion el elemento esta en la lista
removeL :: 	Eq a => a -> [a] -> [a]
removeL x (y:ys) = 
  if x == y
    then ys
	else y: (removeL x ys)
	
-- O(n^2)
-- Dados dos conjuntos devuelve un conjunto con todos 
-- los elementos de ambos. conjuntos.
unionS :: Eq a => Set a -> Set a -> Set a
unionS (UnSet xs size) st = 
  addAll xs st

addAll :: Eq a => [a] -> Set a -> Set a
addAll [] st = st
addAll (x:xs) st = addS x (addAll xs st)

-- O(n^2)
-- Dados dos conjuntos devuelve un conjunto con todos 
-- los elementos en común entre ambos.
intersectionS :: Eq a => Set a -> Set a -> Set a
intersectionS (UnSet xs _) (UnSet ys _) = 
  armarSet (intersectionL xs ys)
  
-- O(n^2)
intersectionL  :: Eq a => [a] -> [a] -> [a]
intersectionL [] ys = [] 
-- recorro la primer lista
intersectionL (x:xs) ys = 
  -- por cada elemento ejecuto elem que es lineal
  if elem x ys
    then x: (intersectionL xs ys)
	else (intersectionL xs ys)
  
-- O(1)
-- Dado un conjunto devuelve una lista con todos los 
-- elementos distintos del conjunto.
setToList :: Eq a => Set a -> [a]
setToList (UnSet xs size) = xs



intercalate :: Show a => String -> [a] -> String
intercalate _ [] = ""
intercalate _ [x] = show x
intercalate separator (x:xs) = 
 (show x) ++ separator ++ (intercalate separator xs)

printSet :: Show a => Set a -> String
printSet (UnSet xs _) =  "{" ++ (intercalate ", " xs) ++ "}"