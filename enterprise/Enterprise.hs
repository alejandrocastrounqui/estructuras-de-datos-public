module Enterprise (
  Rango,
  Tripulante,
  Nave,
  naveVacia,
  tripulantesDe,
  sectores,
  conMayorRango,
  conMasTripulantes,
  conRango,
  sectorDe,
  agregarTripulante
)
where

import Heap
import Map
import Set

data Nave = MkN (Map Sector (Set Tripulante)) (Heap Tripulante) (Sector, Int)

data Sector = S String
data Rango = Almirante | Capitan
data Tripulante = T Rango

rango :: Tripulante -> Rango
rango (T r) = r

-- Propósito: Crea una nave con todos esos sectores sin tripulantes.
-- Precondición: la lista de sectores no está vacía
-- Costo: O(S log S) siendo S la cantidad de sectores de la lista.
naveVacia :: [Sector] -> Nave
naveVacia a = error "Not implemented"

-- Propósito: Obtiene los tripulantes de un sector.
-- Costo: O(log S) siendo S la cantidad de sectores.
tripulantesDe :: Sector -> Nave -> Set Tripulante
tripulantesDe a b = error "Not implemented"

-- Propósito: Denota los sectores de la nave
-- Costo: O(S) siendo S la cantidad de sectores.
sectores :: Nave -> [Sector]
sectores a = error "Not implemented"

-- Propósito: Denota el tripulante con mayor rango.
-- Precondición: la nave no está vacía.
-- Costo: O(1).
conMayorRango :: Nave -> Tripulante
conMayorRango a = error "Not implemented"

-- Propósito: Denota el sector de la nave con más tripulantes.
-- Costo: O(1).
conMasTripulantes :: Nave -> Sector
conMasTripulantes a = error "Not implemented"

-- Propósito: Denota el conjunto de tripulantes con dicho rango.
-- Costo: O(P log P) siendo P la cantidad de tripulantes.
conRango :: Rango -> Nave -> Set Tripulante
conRango a b = error "Not implemented"

-- Propósito: Devuelve el sector en el que se encuentra un tripulante.
-- Precondición: el tripulante pertenece a la nave.
-- Costo: O(S log S log P) siendo S la cantidad de sectores y P la cantidad de tripulantes.
sectorDe :: Tripulante -> Nave -> Sector
sectorDe a b = error "Not implemented"

-- Propósito: Agrega un tripulante a ese sector de la nave.
-- Precondición: El sector está en la nave y el tripulante no.
-- Costo: No hay datos (justifique su elección).
agregarTripulante :: Tripulante -> Sector -> Nave -> Nave
agregarTripulante a b c = error "Not implemented"

