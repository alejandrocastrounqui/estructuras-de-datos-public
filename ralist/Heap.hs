module Heap(
  Heap,
  emptyH,
  isEmptyH,
  findMin,
  insertH,
  deleteMin,
  mkIndex,
  hValue,
  hIndex,
)
where

data HIndex a = TheHIndex Int a
instance Eq a => Eq (HIndex a) where
  (TheHIndex s1 _) == (TheHIndex s2 _) = s1 == s2
instance Ord a => Ord (HIndex a) where
  (TheHIndex s1 _) `compare` (TheHIndex s2 _) = s1 `compare` s2

instance Show a => Show (HIndex a) where
  show (TheHIndex s1 v1)  = show s1 ++ ": " ++ show v1
  
mkIndex n v = TheHIndex n v
hValue (TheHIndex n v) = v
hIndex (TheHIndex n v) = n

data Heap a = TheHeap [a]
instance Show a => Show (Heap a) where
  show (TheHeap xs) = show xs

emptyH :: Heap a
emptyH = TheHeap []

isEmptyH :: Heap a -> Bool
isEmptyH (TheHeap xs) = isEmptyL xs
isEmptyL [] = True

findMin :: Heap a -> a
findMin (TheHeap xs) = findMinL xs

findMinL (x:xs) = x

insertH :: Ord a => a -> Heap a -> Heap a
insertH n (TheHeap xs) = TheHeap (insertL n xs)

insertL n [] = [n]
insertL n (x:xs) = 
  if x > n
     then n:x:xs
     else x:(insertL n xs)

deleteMin :: Ord a => Heap a -> Heap a
deleteMin (TheHeap xs) = TheHeap (deleteMinL xs)
deleteMinL (x:xs) = xs